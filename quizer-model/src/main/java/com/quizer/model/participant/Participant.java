package com.quizer.model.participant;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Participant entity.
 */
@Entity
@Table(name = "PARTICIPANTS")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = "Participants.countAll", query = "Select distinct count(p) from Participant p"),
        @NamedQuery(name = "Participants.findAll", query = "from Participant p"),
        @NamedQuery(name = "Participants.findById", query = "Select p from Participant p where p.id = :id"),
        @NamedQuery(name = "Participants.findByName", query = "Select p from Participant p where p.name = :name"),
        @NamedQuery(name = "Participants.findByIds", query = "Select p from Participant p where p.id in (:idList)"),
        @NamedQuery(name = "Participants.findByNames", query = "Select p from Participant p"
                +
                " where p.name in (:nameList)"),
    })
public class Participant implements Serializable {
    private static final long serialVersionUID = -1064748459177339902L;
    /**
     * Query.
     */
    public static final String COUNT_ALL = "Participants.countAll";
    /**
     * Query.
     */
    public static final String FIND_ALL = "Participants.findAll";
    /**
     * Query.
     */
    public static final String FIND_BY_ID = "Participants.findById";
    /**
     * Query.
     */
    public static final String FIND_BY_NAME = "Participants.findByName";
    /**
     * Query.
     */
    public static final String FIND_BY_IDS = "Participants.findByIds";
    /**
     * Query.
     */
    public static final String FIND_BY_NAMES = "Participants.findByNames";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PART_ID", nullable = false, unique = true, updatable = false, length = 5)
    private Long id;

    @NotNull
    @Size(min = 2, max = 20, message = "Participant's name should be contain between {min} and {max} characters")
    @Column(name = "PART_NAME", unique = true, nullable = false, length = 20)
    private String name;

    /**
     * Non-parametized constructor.
     */
    public Participant() {
        super();
    }

    /**
     * Constructor with String parameter.
     *
     * @param name participant's name
     */
    public Participant(String name) {
        this.name = name;
    }

    /**
     * Retrieves id.
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets new id.
     *
     * @param newId id
     */
    public void setId(Long newId) {
        this.id = newId;
    }

    /**
     * Retrieves participant's name.
     *
     * @return participant's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets participant's name.
     *
     * @param newName participant's name
     */
    public void setName(String newName) {
        this.name = newName;
    }

    /**
     * Returns hashcode.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Checks Whether objects are equal.
     *
     * @param obj to be checked for equality
     * @return result: {@code true} when objects are equal.
     * {@code false} is returned otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Participant)) {
            return false;
        }

        Participant part = (Participant) obj;

        if (!Objects.equals(id, part.id)) {
            return false;
        }

        return Objects.equals(name, part.name);
    }
}

