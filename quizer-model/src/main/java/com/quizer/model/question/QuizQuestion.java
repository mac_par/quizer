package com.quizer.model.question;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Question entity.
 */
@Entity(name = "Question")
@Table(name = "QUESTIONS")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = "Question.countAll", query = "Select distinct count(q) from Question q"),
        @NamedQuery(name = "Question.findAll", query = "from Question q"),
        @NamedQuery(name = "Question.findById", query = "Select q from Question q where q.id = :id"),
        @NamedQuery(name = "Question.findByContent", query = "Select q from Question q where q.content = :content"),
        @NamedQuery(name = "Question.findByCategoryId", query = "Select q from Question q where q.category.id = :id"),
        @NamedQuery(name = "Question.findByCategories", query = "Select q from Question q "
                +
                "where q.category.category IN (:categories)"),
        @NamedQuery(name = "Question.findByIds", query = "Select q from Question q where q.id in (:idList)"),
        @NamedQuery(name = "Question.findByCategoryIds", query = "Select q from Question q "
                +
                "where q.category.id in (:categoryIdList)"),
    })
public class QuizQuestion implements Serializable {
    private static final long serialVersionUID = 6621961743966542427L;

    /**
     * Query.
     */
    public static final String COUNT_ALL = "Question.countAll";
    /**
     * Query.
     */
    public static final String FIND_ALL = "Question.findAll";
    /**
     * Query.
     */
    public static final String FIND_BY_ID = "Question.findById";
    /**
     * Query.
     */
    public static final String FIND_BY_CONTENT = "Question.findByContent";
    /**
     * Query.
     */
    public static final String FIND_BY_CATEGORY_ID = "Question.findByCategoryId";
    /**
     * Query.
     */
    public static final String FIND_BY_CATEGORY = "Question.findByCategories";
    /**
     * Query.
     */
    public static final String FIND_BY_IDS = "Question.findByIds";
    /**
     * Query.
     */
    public static final String FIND_BY_CATEGORY_IDS = "Question.findByCategoryIds";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "QUESTION_ID", scale = 5, nullable = false, unique = true, updatable = false)
    private Long id;

    @NotNull
    @Size(min = 8, max = 80, message = "Question should be between {min} and {max} characters")
    @Column(name = "QUESTION_CONTENT", unique = true, nullable = false, length = 80)
    private String content;

    @NotNull
    @Lob
    @Size(min = 8, message = "Answer should be at least of {min} characters")
    @Column(name = "QUESTION_ANSWER", unique = true, nullable = false)
    private String answer;

    @NotNull
    @OneToOne(optional = false, cascade = {CascadeType.REFRESH, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "CATEGORY_ID")
    private QuizQuestionCategory category;

    /**
     * Non-parametized constructor.
     */
    public QuizQuestion() {
        super();
    }

    /**
     * Constructor that accepts content and category.
     *
     * @param initContent  question content
     * @param initCategory question category
     */
    public QuizQuestion(String initContent, QuizQuestionCategory initCategory) {
        this.content = initContent;
        this.category = initCategory;
    }

    /**
     * Retrieves question id.
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets question id.
     *
     * @param newId id
     */
    public void setId(Long newId) {
        this.id = newId;
    }

    /**
     * Retrieves content.
     *
     * @return question content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets question content.
     *
     * @param newContent content
     */
    public void setContent(String newContent) {
        this.content = newContent;
    }

    /**
     * Retrieves question's category.
     *
     * @return category
     */
    public QuizQuestionCategory getCategory() {
        return category;
    }

    /**
     * Sets question's category.
     *
     * @param newCategory category
     */
    public void setCategory(QuizQuestionCategory newCategory) {
        this.category = newCategory;
    }

    /**
     * Retrieves an answer for the question.
     *
     * @return an answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Sets new answer.
     *
     * @param newAnswer new answer
     */
    public void setAnswer(String newAnswer) {
        this.answer = newAnswer;
    }

    /**
     * Retrieves hashcode.
     *
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    /**
     * Checks for equality with object.
     *
     * @param obj object to be checked for equaluty
     * @return result: {@code true} if objects are equal. {@code false} otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof QuizQuestion)) {
            return false;
        }

        QuizQuestion question = (QuizQuestion) obj;
        if (!Objects.equals(id, question.id)) {
            return false;
        }

        if (!Objects.equals(content, question.content)) {
            return false;
        }

        return Objects.equals(category, question.category);
    }
}
