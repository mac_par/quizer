package com.quizer.model.question;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Category entity.
 */
@Entity(name = "Category")
@Table(name = "CATEGORIES")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = "Category.countAll", query = "Select distinct count(c) from Category c"),
        @NamedQuery(name = "Category.findAll", query = "from Category c"),
        @NamedQuery(name = "Category.findById", query = "Select c from Category c where c.id = :id"),
        @NamedQuery(name = "Category.findByCategory", query = "Select c from Category c where c.category = :category"),
        @NamedQuery(name = "Category.findByCategories", query = "Select c from Category c"
                +
                " where c.category in (:categoriesList)"),
        @NamedQuery(name = "Category.findByIds", query = "Select c from Category c where c.id in (:idList)"),
    })
public class QuizQuestionCategory implements Serializable {
    private static final long serialVersionUID = -5775213663736945556L;

    /**
     * Query.
     */
    public static final String COUNT_ALL = "Category.countAll";
    /**
     * Query.
     */
    public static final String FIND_ALL = "Category.findAll";
    /**
     * Query.
     */
    public static final String FIND_BY_ID = "Category.findById";
    /**
     * Query.
     */
    public static final String FIND_BY_CATEGORY = "Category.findByCategory";
    /**
     * Query.
     */
    public static final String FIND_BY_CATEGORIES = "Category.findByCategories";
    /**
     * Query.
     */
    public static final String FIND_BY_IDS = "Category.findByIds";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CATEGORY_ID", nullable = false, unique = true, scale = 5, updatable = false)
    private Long id;

    @NotNull
    @Size(max = 25, min = 2, message = "QuizQuestionCategory should be between {min} and {max} characters")
    @Column(name = "CATEGORY_NAME", unique = true, nullable = false, length = 25)
    private String category;

    /**
     * Non-parametrized constructor.
     */
    public QuizQuestionCategory() {
        super();
    }

    /**
     * Parametrized constructor with label.
     *
     * @param label quiz category
     */
    public QuizQuestionCategory(String label) {
        this.category = label;
    }

    /**
     * Retrieves quiz category id.
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets new id.
     *
     * @param newId id
     */
    public void setId(Long newId) {
        this.id = newId;
    }

    /**
     * Retrieves quiz category.
     *
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets quiz category.
     *
     * @param newCategory quiz category
     */
    public void setCategory(String newCategory) {
        this.category = newCategory;
    }

    /**
     * Retrieves hashCode.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int hash = 0;
        if (!Objects.isNull(this.id)) {
            hash = id.hashCode();
        }
        if (!Objects.isNull(this.category)) {
            hash += this.category.hashCode();
        }
        return hash;
    }

    /**
     * Checks Whether objects are equal.
     *
     * @param obj to be checked for equality
     * @return result: {@code true} when objects are equal.
     * {@code false} is returned otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QuizQuestionCategory)) {
            return false;
        }

        QuizQuestionCategory quiz = (QuizQuestionCategory) obj;
        if (!Objects.equals(id, quiz.id)) {
            return false;
        }

        return Objects.equals(category, quiz.category);
    }

    /**
     * Retrives String representation of the instance.
     *
     * @return String representation
     */
    @Override
    public String toString() {
        return category;
    }
}
