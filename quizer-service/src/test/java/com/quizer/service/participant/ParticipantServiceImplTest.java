package com.quizer.service.participant;

import com.quizer.model.participant.Participant;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ParticipantServiceImplTest {
    private static final String PARTICIPANTS_PROVIDER = "ALL_PARTICIPANTS";
    private static final String SELECTED_PARTS = "SOME guys";
    private static final String BY_NAME = "PartByName";
    private static final String BY_NAMES = "PartsBNames";
    private static ParticipantService service;

    @BeforeClass
    public static void setUp() {
        service = new ParticipantServiceImpl();
    }

    @DataProvider(name = PARTICIPANTS_PROVIDER)
    public Object[][] getParticipants() {
        return new Object[][]{
                {1L, "Marco Polo"},
                {2L, "Jozef Stalin"},
                {3L, "Baphomet"},
                {4L, "Lucifer MorningStar"},
                {5L, "Ibn al Muqaffa3"},
                {6L, "Donald Trump"},
                {7L, "Bashar al Asad"},
                {8L, "Marian Paździoch"},
                {9L, "Kłapouchy"},
        };
    }

    @DataProvider(name = SELECTED_PARTS)
    public Object[][] selectedParts() {
        return new Object[][]{
                {List.of(1L, 5L, 10L), List.of("Marco Polo", "Ibn al Muqaffa3")},
                {List.of(2L, 9L, 7L, 15L), List.of("Jozef Stalin", "Kłapouchy", "Bashar al Asad")},
                {List.of(4L, 9L, 7L, 8L), List.of("Lucifer MorningStar", "Kłapouchy", "Bashar al Asad", "Marian Paździoch")},
        };
    }


    @DataProvider(name = BY_NAME)
    public Object[][] getParticipantsRev() {
        return new Object[][]{
                {"Marco Polo", 1L},
                {"Jozef Stalin", 2L},
                {"Baphomet", 3L},
                {"Lucifer MorningStar", 4L},
                {"Ibn al Muqaffa3", 5L},
                {"Donald Trump", 6L},
                {"Bashar al Asad", 7L},
                {"Marian Paździoch", 8L},
                {"Kłapouchy", 9L},
        };
    }

    @DataProvider(name = BY_NAMES)
    public Object[][] selectedPartsRev() {
        return new Object[][]{
                {List.of("Marco Polo", "Ibn al Muqaffa3"), List.of(1L, 5L)},
                {List.of("Jozef Stalin", "Kłapouchy", "Bashar al Asad"), List.of(2L, 9L, 7L)},
                {List.of("Lucifer MorningStar", "Kłapouchy", "Bashar al Asad", "Marian Paździoch"), List.of(4L, 9L, 7L, 8L)},
        };
    }

    @Test
    public void retrieveCount() {
        Long count = service.count();
        final Long amountOfParticipants = 9L;
        assertThat(count).isNotNull();
        assertThat(count).isNotEqualTo(Long.MAX_VALUE);
        assertThat(count).isEqualTo(amountOfParticipants);
    }

    @Test(dataProvider = PARTICIPANTS_PROVIDER)
    public void findById(Long id, String name) {
        //When
        //Given
        //Then
        Optional<Participant> participantOptional = service.findById(id);
        assertThat(participantOptional).isNotEmpty();
        assertThat(participantOptional.get().getName()).isEqualTo(name);
    }

    @Test
    public void findByIdOnUnknowIdReturnsEmptyOptional() {
        //When
        //Given
        final Long unknown = 15L;
        //Then
        Optional<Participant> participant = service.findById(unknown);
        assertThat(participant).isEmpty();
    }

    @Test(dataProvider = SELECTED_PARTS)
    public void byIdsListParticipantsAreRetrieved(List<Long> ids, List<String> parts) {
        //When
        //Given
        //Then
        List<Participant> result = service.findByIds(ids);
        List<String> names = result.stream().map(p -> p.getName()).collect(Collectors.toList());

        assertThat(names).isNotEmpty();
        assertThat(names).containsAll(parts);
    }

    @Test
    public void byNonExistingIdsWillBeEmpty() {
        //When
        //Given
        List<Long> longs = List.of(15L, 25L, 0L, 45L);
        //Then

        List<Participant> parts = service.findByIds(longs);
        List<String> names = parts.stream().map(p -> p.getName()).collect(Collectors.toList());

        assertThat(names).isEmpty();
    }

    @Test(dataProvider = BY_NAME)
    public void existingNameCanLeadToParicipiant(String name, Long partId) {
        //When
        //Given
        //Then
        Optional<Participant> participant = service.findByName(name);
        assertThat(participant).isNotEmpty();
        assertThat(participant.get().getId()).isEqualTo(partId);
    }

    @Test(dataProvider = BY_NAMES)
    public void existingNameaCanLeadToParicipiants(List<String> names, List<Long> ids) {
        //When
        //Given
        //Then
        List<Participant> parts = service.findByNames(names);
        List<Long> resultIds = parts.stream().map(Participant::getId).collect(Collectors.toList());
        assertThat(resultIds).isNotEmpty();
        assertThat(resultIds).containsAll(ids);
    }

    @Test
    public void findAllShouldRetrieveAllPrticipants() {
        //When
        Long maxItems = 9L;
        //Given
        //Then
        List<Participant> list = service.findAll();
        assertThat(list).isNotNull();
        assertThat(list).isNotEmpty();
        assertThat(list).hasSize(maxItems.intValue());
    }
}