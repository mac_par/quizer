package com.quizer.service.question;

import com.quizer.model.question.QuizQuestionCategory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class QuizQuestionCategoryServiceImplTest {
    private static final String NOT_EXISTENT = "DOES NOT_EXIST";
    private static final String EXISTING_IDS = "doesExist";
    private static final String VARIOUS_IDS = "rozne idkiki";
    private static final String BY_CATEGORY = "byCat";
    private static final String BY_CATEGORIES = "byCats";
    private static QuizQuestionCategoryService SERVICE;


    @DataProvider(name = NOT_EXISTENT)
    public Object[][] getNotExistentId() {
        return new Object[][]{
                {9L}, {15L}, {21L}
        };
    }

    @DataProvider(name = EXISTING_IDS)
    public Object[][] getId() {
        return new Object[][]{
                {1L, "Lifestyle"},
                {2L, "Politics"},
                {3L, "Bribery"},
                {4L, "Entertainment"},
                {5L, "Narcotics"},
                {6L, "Idmans"},
                {7L, "Religion"},
                {8L, "Celestrial Beings"},
        };
    }

    @DataProvider(name = VARIOUS_IDS)
    public Object[][] getVariousIds() {
        return new Object[][]{
                {List.of(1L, 7L, 5L), 3},
                {List.of(10L, 17L, 4L), 1},
                {List.of(3L, 2L, 6L, 5L), 4},
                {List.of(13L, 12L, 16L, 15L), 0},
        };
    }

    @DataProvider(name = BY_CATEGORY)
    public Object[][] getCategoryByName() {
        return new Object[][]{
                {"Lifestyle", 1L},
                {"Politics", 2L},
                {"Bribery", 3L},
                {"Entertainment", 4L},
                {"Narcotics", 5L},
                {"Idmans", 6L},
                {"Religion", 7L},
                {"Celestrial Beings", 8L},
        };
    }

    @DataProvider(name = BY_CATEGORIES)
    public Object[][] getCategoriesByNames() {
        return new Object[][]{
                {List.of("Lifestyle", "Politics", "Kitchen"), 2},
                {List.of("Bribery", "Be Kosher", "Idmans"), 2},
                {List.of("Entertainment", "Narcotics", "Celestrial Beings", "Religion"), 4},
                {List.of("Cos1", "Cos2", "cos4", "Cos tam1"), 0},
        };
    }

    @BeforeClass
    public static void setUp() {
        SERVICE = new QuizQuestionCategoryServiceImpl();
    }

    @AfterClass
    public static void tearDown() {
        SERVICE = null;
    }

    @Test
    public void countShouldReturnActualAmountOfCategories() {
        //When
        //Given
        final Long expectedResult = 8L;
        //Then
        final Long result = SERVICE.count();

        assertThat(result).isNotNull();
        assertThat(result).isNotEqualTo(Long.MAX_VALUE);
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void retrievesAllElementsFromCategories() {
        //When
        //Given
        List<String> categories = List.of("Lifestyle", "Politics", "Bribery", "Entertainment",
                "Narcotics", "Idmans", "Religion", "Celestrial Beings");
        //Then
        List<QuizQuestionCategory> result = SERVICE.findAll();
        List<String> resultCategories = result.stream()
                .map(QuizQuestionCategory::getCategory)
                .collect(Collectors.toList());

        assertThat(result).isNotEmpty();
        assertThat(resultCategories).containsAll(categories);
    }

    @Test(dataProvider = NOT_EXISTENT)
    public void notExistingIdShouldBeReflectedByEmptyOptional(Long id) {
        //When
        //Given
        //Then
        Optional<QuizQuestionCategory> optional = SERVICE.findById(id);
        assertThat(optional).isEmpty();
    }

    @Test(dataProvider = EXISTING_IDS)
    public void notExistingIdShouldBeReflectedByEmptyOptional(Long id, String label) {
        //When
        //Given
        //Then
        Optional<QuizQuestionCategory> optional = SERVICE.findById(id);
        assertThat(optional).isNotEmpty();
        assertThat(optional.get().getCategory()).isEqualTo(label);
    }

    @Test(dataProvider = VARIOUS_IDS)
    public void onlyExistingIdsCanProvideCategories(List<Long> ids, Integer size) {
        //When
        //Given
        //Then
        List<QuizQuestionCategory> result = SERVICE.findByIds(ids);
        assertThat(result).hasSize(size);
    }

    @Test(dataProvider = BY_CATEGORY)
    public void categoryNameCanBeUsedToExtractCategory(String label, Long id) {
        //When
        //Given
        //Then
        Optional<QuizQuestionCategory> optionalCategory = SERVICE.findByCategory(label);
        assertThat(optionalCategory).isNotEmpty();
        assertThat(optionalCategory.get().getId()).isEqualTo(id);
    }

    @Test
    public void setNotExistentCategoryNameWontHelpRetrieveCategory() {
        //When
        final String category = "Something not existent";
        //Given
        //Then
        Optional<QuizQuestionCategory> optionalCategory = SERVICE.findByCategory(category);
        assertThat(optionalCategory).isEmpty();
    }

    @Test(dataProvider = BY_CATEGORIES)
    public void onlyByExistingLabelsCategoriesAreRetrieved(List<String> labels, int size) {
        List<QuizQuestionCategory> result = SERVICE.findByCategories(labels);
        assertThat(result).isNotNull();
        assertThat(result).hasSize(size);
    }
}