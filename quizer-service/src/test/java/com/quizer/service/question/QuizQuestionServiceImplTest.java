package com.quizer.service.question;

import com.quizer.model.question.QuizQuestion;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class QuizQuestionServiceImplTest {
    private static final String Q_BY_CNT = "getByContent";
    private static final String Q_BY_CAT_ID = "getQuestionByCategoryId";
    private static final String Q_BY_CAT_IDS = "getQuestionsByCategoryIds";
    private static final String Q_BY_CAT_NAMES = "CategoryNames";
    private static QuizQuestionService SERVICE;
    private static final String Q_BY_ID = "instanceById";
    private static final String QS_BY_IDS = "instanceByIds";

    @BeforeClass
    public static void setUp() {
        SERVICE = new QuizQuestionServiceImpl();
    }

    @AfterClass
    public static void tearDown() {
        SERVICE = null;
    }

    @DataProvider(name = Q_BY_ID)
    public Object[][] getId() {
        return new Object[][]{
                {1L, "Is Fish & Chips a healthy dish?", 1L},
                {2L, "Is starvation benefitial for longer that a year?", 1L},
                {3L, "What is BDSM?", 1L},
                {4L, "Who is responsible for Nuclear Deal issue?", 2L},
                {5L, "Will Trump be re-elected?", 2L},
                {6L, "How long will Saudis be quiet about Khashuqji?", 2L},
                {7L, "How much could you take?", 3L},
                {8L, "Sex for money, is it sill a sex?", 3L},
                {9L, "How much do i need to get higher education?", 3L},
                {10L, "Who the fuck is Doctor Who?", 4L},
                {11L, "Why Junction 48 is not available on Polish Netflix?", 4L},
                {12L, "Why Law TV-series is not available on Netflix?", 4L},
                {13L, "How much is caugh syrop?", 5L},
                {14L, "Do you need a Xanax pill?", 5L},
                {15L, "Where can i find Nefedron?", 5L},
                {16L, "Can you stop drinking for a week?", 6L},
                {17L, "Is staying up late an addiction?", 6L},
                {18L, "I hate people, could it be an addition to being alone?", 6L},
                {19L, "Where is God?", 7L},
                {20L, "Why Adonai did not do anything with the parlamanet?", 7L},
                {21L, "Is Lucifer real?", 7L},
                {22L, "What is a black hole?", 8L},
                {23L, "What is Mammon?", 8L},
                {24L, "How many children God had?", 8L},
        };
    }

    @DataProvider(name = Q_BY_CNT)
    public Object[][] getByContent() {
        return new Object[][]{
                {"Is starvation benefitial for longer that a year?", 2L,},
                {"How long will Saudis be quiet about Khashuqji?", 6L},
                {"Does hearing hear a lot?", null},
                {"Who the fuck is Doctor Who?", 10L},
                {"Do you need a Xanax pill?", 14L},
                {"Is maślanka a proof of God's existence?", null},
                {"Why Adonai did not do anything with the parlamanet?", 20L},
                {"What is Mammon?", 23L},
        };
    }

    @DataProvider(name = QS_BY_IDS)
    public Object[][] getIDs() {
        return new Object[][]{
                {List.of(6L, 22L, 24L), 3},
                {List.of(55L, 33L, 34L), 0},
                {List.of(20L, 34L), 1},
                {List.of(4L, 12L, 8L, 24L), 4},
                {List.of(4L, 12L, 8L, 24L, 24L, 8L, 12L, 4L), 4},
        };
    }

    @DataProvider(name = Q_BY_CAT_ID)
    public Object[][] getQuestionByCategoryId() {
        return new Object[][]{
                {1L, 3},
                {0L, 0},
                {3L, 3},
                {4L, 3},
                {45L, 0},
                {7L, 3},
                {8L, 3},
                {100L, 0},
        };
    }

    @DataProvider(name = Q_BY_CAT_IDS)
    public Object[][] getQuestionByCategoryName() {
        return new Object[][]{
                {List.of(1L, 3L), 6},
                {List.of(0L), 0},
                {List.of(3L), 3},
                {List.of(4L, 8L), 6},
                {List.of(45L, 7L, 11L), 3},
                {List.of(100L, 120L), 0},
        };
    }

    @DataProvider(name = Q_BY_CAT_NAMES)
    public Object[][] getQuestionByCategoryIds() {
        return new Object[][]{
                {List.of("Lifestyle", "Bribery"), 6},
                {List.of("Mean and Greedy"), 0},
                {List.of("Narcotics"), 3},
                {List.of("Entertainment", "Celestrial Beings"), 6},
                {List.of("Mamma mia", "Religion", "Mialko"), 3},
                {List.of("Pixie", "Trixie"), 0},
        };
    }

    @Test
    public void countRetrievesTotalCount() {
        final long expectedCount = 24L;
        Long result = SERVICE.count();

        assertThat(result).isEqualTo(expectedCount);
    }

    @Test
    public void findAllShouldRetrieveAllItems() {
        //When
        final int size = 24;
        final long expectedAmountOfNull = 0;
        //Given
        //Then
        List<QuizQuestion> result = SERVICE.findAll();
        final long resultNulls = result.stream().filter(q -> q.getCategory() == null).count();

        assertThat(result).isNotEmpty();
        assertThat(result).hasSize(size);
        assertThat(resultNulls).isEqualTo(expectedAmountOfNull);
    }

    @Test(dataProvider = Q_BY_ID)
    public void fullEntityShouldBeRetrivedByItsId(Long id, String content, Long categoryId) {
        //When
        //Given
        //Then
        Optional<QuizQuestion> question = SERVICE.findById(id);
        assertThat(question).isNotEmpty();
        assertThat(question.get().getContent()).isEqualTo(content);
        assertThat(question.get().getCategory().getId()).isEqualTo(categoryId);
    }

    @Test(dataProvider = QS_BY_IDS)
    public void lookupByUnexistingIdWillNotWork(List<Long> ids, int size) {

        List<QuizQuestion> result = SERVICE.findByIds(ids);
        assertThat(result).isNotNull();
        assertThat(result).hasSize(size);
    }

    @Test(dataProvider = Q_BY_CNT)
    public void byValidContentInstanceCanBeRetrived(String content, Long id) {
        //When
        //Given
        //Then
        Optional<QuizQuestion> question = SERVICE.findByContent(content);
        if (id != null) {
            assertThat(question).isNotEmpty();
            assertThat(question.get().getId()).isEqualTo(id);
        } else {
            assertThat(question).isEmpty();
        }
    }

    @Test(dataProvider = Q_BY_CAT_ID)
    public void onlyExistingCategoryIdsCanBeUsedToRetrieveQuestions(Long catId, int size) {
        List<QuizQuestion> question = SERVICE.findByCategoryId(catId);
        assertThat(question).hasSize(size);
    }

    @Test(dataProvider = Q_BY_CAT_IDS)
    public void onlyExistingCategoriesIdsCanBeUsedToRetrieveQuestions(List<Long> ids, int size) {
        List<QuizQuestion> questions = SERVICE.findByCategoryIds(ids);
        assertThat(questions).hasSize(size);
        checkQuizQuestionAnswer(questions);
    }

    @Test(dataProvider = Q_BY_CAT_NAMES)
    public void onlyByExistingCategoryNamesQuestionsAreRetrived(List<String> names, int size) {
        List<QuizQuestion> questions = SERVICE.findByCategories(names);
        assertThat(questions).isNotNull();
        assertThat(questions).hasSize(size);
        checkQuizQuestionAnswer(questions);
    }

    private void checkQuizQuestionAnswer(List<QuizQuestion> questions) {
        for (QuizQuestion question : questions) {
            assertThat(question.getAnswer()).isNotNull();
        }
    }
}