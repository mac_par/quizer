package com.quizer.service.components;

import org.assertj.core.api.Assertions;
import org.hibernate.Session;
import org.testng.annotations.Test;

public class SessionUtilityTest {
    @Test
    public void sessionShouldBeReceived() {
        try (Session session = SessionUtility.getSession()) {
            Assertions.assertThat(session).isNotNull();
        }
    }
}