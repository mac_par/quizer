package com.quizer.service.participant;

import com.quizer.model.participant.Participant;

import java.util.List;
import java.util.Optional;

/**
 * Provides basic api for retrieval of Participant entities.
 */
public interface ParticipantService {
    /**
     * Retrieves amount of participants.
     *
     * @return amount
     */
    Long count();

    /**
     * Retrieves Participant instance by id.
     *
     * @param id looked id
     * @return Optional with Participant instance or empty Optional
     */
    Optional<Participant> findById(Long id);

    /**
     * Retrieves list of participants by their ids.
     *
     * @param ids list of ids
     * @return list of Participants
     */
    List<Participant> findByIds(List<Long> ids);

    /**
     * Retrieves Participant by name.
     *
     * @param name looked name
     * @return Optional with Participant instance or empty Optional
     */
    Optional<Participant> findByName(String name);

    /**
     * Retrieves Participants by names.
     *
     * @param names looked names
     * @return list of Participants
     */
    List<Participant> findByNames(List<String> names);

    /**
     * Retrieves all participants.
     * @return list
     */
    List<Participant> findAll();
}
