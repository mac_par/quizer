package com.quizer.service.participant;

import com.quizer.model.participant.Participant;
import com.quizer.service.components.BaseService;
import org.hibernate.Session;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * ParticipantService implementation.
 */
public class ParticipantServiceImpl extends BaseService implements ParticipantService {
    /**
     * Non-parametized constructor.
     */
    public ParticipantServiceImpl() {
        super(LoggerFactory.getLogger(MethodHandles.lookup().lookupClass()));
    }

    /**
     * Retrieves amount of participants.
     *
     * @return amount
     */
    public Long count() {
        try (Session session = openSession()) {
            return session.createNamedQuery(Participant.COUNT_ALL, Long.class)
                    .getSingleResult();
        } catch (RuntimeException ex) {
            logWarn("findById", ex);
            return Long.MAX_VALUE;
        }
    }

    /**
     * Retrieves Participant instance by id.
     *
     * @param id looked id
     * @return Optional<Participant> instance
     */
    public Optional<Participant> findById(Long id) {
        try (Session session = openSession()) {
            return Optional.of(
                    session
                            .createNamedQuery(Participant.FIND_BY_ID, Participant.class)
                            .setParameter("id", id)
                            .getSingleResult());
        } catch (RuntimeException ex) {
            logWarn("findById", ex);
            return Optional.empty();
        }
    }

    /**
     * Retrieves list of participants by their ids.
     *
     * @param ids list of ids
     * @return list of Participants
     */
    public List<Participant> findByIds(List<Long> ids) {
        logInfo("Retrieving entities by ids");
        try (Session session = openSession()) {
            List<Participant> list = session
                    .createNamedQuery(Participant.FIND_BY_IDS, Participant.class)
                    .setParameter("idList", ids)
                    .getResultList();
            logInfo("findByIds: retrieved {} items", list.size());
            return list;
        }
    }

    /**
     * Retrieves Participant by name.
     *
     * @param name looked name
     * @return Optional<Participant> instance
     */
    public Optional<Participant> findByName(String name) {
        try (Session session = openSession()) {
            return Optional.of(session
                    .createNamedQuery(Participant.FIND_BY_NAME, Participant.class)
                    .setParameter("name", name)
                    .getSingleResult());
        } catch (RuntimeException ex) {
            logWarn("findByName", ex);
            return Optional.empty();
        }
    }

    /**
     * Retrieves Participants by names.
     *
     * @param names looked names
     * @return list of Participants
     */
    public List<Participant> findByNames(List<String> names) {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(Participant.FIND_BY_NAMES, Participant.class)
                    .setParameter("nameList", names)
                    .getResultList();
        }
    }

    /**
     * Retrieves all participants.
     * @return list with participants
     */
    @Override
    public List<Participant> findAll() {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(Participant.FIND_ALL, Participant.class)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findAll", ex);
            return Collections.emptyList();
        }
    }
}
