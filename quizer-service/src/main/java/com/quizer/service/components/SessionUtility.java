package com.quizer.service.components;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectStreamException;
import java.lang.invoke.MethodHandles;
import java.util.Objects;
import java.util.concurrent.Semaphore;

/**
 * {@code SessionUtility} provides functionalities related with datasource session.
 */
final class SessionUtility implements Cloneable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final Semaphore SEMAPHORE = new Semaphore(1, true);

    private static SessionUtility instance;
    private SessionFactory sessionFactory;


    private SessionUtility() {
        StandardServiceRegistry serviceRegistry = null;
        try {
            Configuration cfg = new Configuration().configure("hbm/hbm.cfg.xml");
            serviceRegistry = cfg.getStandardServiceRegistryBuilder().build();
            this.sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
            LOGGER.info("SessionFactory was successfully initialized");
        } catch (RuntimeException ex) {
            LOGGER.warn("Ctor", ex);
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }

    private static SessionUtility getInstance() {
        try {
            SEMAPHORE.acquire();
            if (Objects.isNull(instance)) {
                instance = new SessionUtility();
            }
            return instance;
        } catch (Exception e) {
            LOGGER.error("getInstance", e);
            return null;
        } finally {
            SEMAPHORE.release();
        }
    }

    /**
     * Creates Session instance.
     *
     * @return Session instance
     */
    public static Session getSession() {
        Session session = getInstance().getSessionFactory().openSession();
        LOGGER.info("Session was opened: {}", session != null);
        return session;
    }

    private SessionFactory getSessionFactory() {
        Objects.requireNonNull(sessionFactory);
        return sessionFactory;
    }
    /**
     * zzzz
     * @return zzz
     * @throws CloneNotSupportedException zz
     */
    @Override
    public SessionUtility clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private Object readResolve() throws ObjectStreamException {
        return instance;
    }
}
