package com.quizer.service.components;

import org.hibernate.Session;
import org.slf4j.Logger;

import java.util.function.Consumer;

/**
 * Class with Service basic functionalities.
 */
public class BaseService {
    private final Logger logger;

    /**
     * zzzzzzzzzzz.
     * @param log zzzzzzzzzzz
     */
    protected BaseService(Logger log) {
        this.logger = log;
    }

    /**
     * zzzzzzzzz.
     * @return zzzzzzzzzzzz
     */
    protected Session openSession() {
        logInfo("Creating session");
        return SessionUtility.getSession();
    }

    /**
     * zzzzzzzzzzzzz.
     * @param action zzzzzzzzzzz
     */
    protected void perform(Consumer<Session> action) {
        try (Session session = openSession()) {
            action.accept(session);
        } catch (RuntimeException ex) {
            logWarn("perform", ex);
        }
    }
    /**
     * zzz.
     * @param msg zz
     * @param args zz
     */
    protected void logInfo(String msg, Object... args) {
        logger.info(msg, args);
    }

    /**
     * zzzzz.
     * @param msg zz
     * @param throwable zz
     */
    protected void logWarn(String msg, RuntimeException throwable) {
        logger.warn(msg, throwable);
    }
}
