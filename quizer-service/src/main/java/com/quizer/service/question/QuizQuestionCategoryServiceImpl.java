package com.quizer.service.question;

import com.quizer.model.question.QuizQuestionCategory;
import com.quizer.service.components.BaseService;
import org.hibernate.Session;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Service provides QuizQuestionCategoryServiceImpl-related functionalities.
 * Only read-only mode is allowed.
 */
public class QuizQuestionCategoryServiceImpl extends BaseService implements QuizQuestionCategoryService {
    /**
     * Non-parametized constructor.
     */
    public QuizQuestionCategoryServiceImpl() {
        super(LoggerFactory.getLogger(MethodHandles.lookup().lookupClass()));
    }

    @Override
    public Long count() {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestionCategory.COUNT_ALL, Long.class)
                    .getSingleResult();
        } catch (RuntimeException ex) {
            logWarn("count", ex);
            return Long.MAX_VALUE;
        }
    }

    @Override
    public List<QuizQuestionCategory> findAll() {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestionCategory.FIND_ALL, QuizQuestionCategory.class)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findAll", ex);
            return Collections.emptyList();
        }
    }

    @Override
    public List<QuizQuestionCategory> findByIds(List<Long> categories) {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestionCategory.FIND_BY_IDS, QuizQuestionCategory.class)
                    .setParameter("idList", categories)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findByIds", ex);
            return Collections.emptyList();
        }
    }

    @Override
    public Optional<QuizQuestionCategory> findById(Long id) {
        try (Session session = openSession()) {
            return Optional.of(
                    session
                            .createNamedQuery(QuizQuestionCategory.FIND_BY_ID, QuizQuestionCategory.class)
                            .setParameter("id", id)
                            .getSingleResult());
        } catch (RuntimeException ex) {
            logWarn("findById", ex);
            return Optional.empty();
        }
    }

    @Override
    public Optional<QuizQuestionCategory> findByCategory(String category) {
        try (Session session = openSession()) {
            return Optional.of(
                    session
                            .createNamedQuery(QuizQuestionCategory.FIND_BY_CATEGORY, QuizQuestionCategory.class)
                            .setParameter("category", category)
                            .getSingleResult());
        } catch (RuntimeException ex) {
            logWarn("findByCategory", ex);
            return Optional.empty();
        }
    }

    @Override
    public List<QuizQuestionCategory> findByCategories(List<String> categories) {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestionCategory.FIND_BY_CATEGORIES, QuizQuestionCategory.class)
                    .setParameter("categoriesList", categories)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findByCategories", ex);
            return Collections.emptyList();
        }
    }
}
