package com.quizer.service.question;

import com.quizer.model.question.QuizQuestion;

import java.util.List;
import java.util.Optional;

/**
 * QuizQuestionService provides api for service.
 */
public interface QuizQuestionService {
    /**
     * Retrieves items count.
     *
     * @return amount
     */
    Long count();

    /**
     * Retrieves all items.
     *
     * @return list of items
     */
    List<QuizQuestion> findAll();

    /**
     * Retrieves an item by id.
     *
     * @param id looked id
     * @return {@code Optional} with item or empty Optional
     */
    Optional<QuizQuestion> findById(Long id);

    /**
     * Retrieves list of items by ids.
     *
     * @param ids looked ids
     * @return list of items
     */
    List<QuizQuestion> findByIds(List<Long> ids);

    /**
     * Retrieves item by question content.
     *
     * @param content question content in form of {@code java.lang.String}
     * @return {@code Optional} with item or empty Optional
     */
    Optional<QuizQuestion> findByContent(String content);

    /**
     * Retrieves item by category id.
     *
     * @param id looked category id
     * @return list of questions
     */
    List<QuizQuestion> findByCategoryId(Long id);

    /**
     * Retrieves list of items by question category ids.
     *
     * @param ids looke category ids
     * @return list of questions
     */
    List<QuizQuestion> findByCategoryIds(List<Long> ids);

    /**
     * Retrieves an item by Category instance.
     *
     * @param questionCategories question category
     * @return list
     */
    List<QuizQuestion> findByCategories(List<String> questionCategories);
}
