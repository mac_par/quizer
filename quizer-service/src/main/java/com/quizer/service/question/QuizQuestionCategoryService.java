package com.quizer.service.question;

import com.quizer.model.question.QuizQuestionCategory;

import java.util.List;
import java.util.Optional;

/**
 * QuizQuestionCategoryService provides api for retrival of items.
 */
public interface QuizQuestionCategoryService {

    /**
     * Retrieves total amount of QuizQuestionCategories.
     *
     * @return amount of categories
     */
    Long count();

    /**
     * Retrieves all quizQuestionCategories.
     *
     * @return list of instances
     */
    List<QuizQuestionCategory> findAll();

    /**
     * Retrieves QuizQuestionCategory on basis of multiple ids.
     *
     * @param categories list of categories
     * @return list of QuizQuestionCategories
     */
    List<QuizQuestionCategory> findByIds(List<Long> categories);

    /**
     * Retrieves a QuizQuestionCategory by id.
     *
     * @param id looked id
     * @return {@code Optional} instance with object instance or empty Optional
     */
    Optional<QuizQuestionCategory> findById(Long id);

    /**
     * Retrieves a QuizQuestionCategory by a string representing category.
     *
     * @param category looked id
     * @return {@code Optional} instance with object instance or empty Optional
     */
    Optional<QuizQuestionCategory> findByCategory(String category);

    /**
     * Retrieves a QuizQuestionCategory by collection of ids.
     *
     * @param categories list of ids
     * @return {@code Optional} instance with object instance or empty Optional
     */
    List<QuizQuestionCategory> findByCategories(List<String> categories);

}
