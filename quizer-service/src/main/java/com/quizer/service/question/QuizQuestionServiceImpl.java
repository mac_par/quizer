package com.quizer.service.question;

import com.quizer.model.question.QuizQuestion;
import com.quizer.service.components.BaseService;
import org.hibernate.Session;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * QuizQuestionService implementation.
 */
public class QuizQuestionServiceImpl extends BaseService implements QuizQuestionService {
    /**
     * Non-parametized constructor.
     */
    public QuizQuestionServiceImpl() {
        super(LoggerFactory.getLogger(MethodHandles.lookup().lookupClass()));
    }

    /**
     * Retrieves items count.
     *
     * @return amount
     */
    @Override
    public Long count() {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestion.COUNT_ALL, Long.class)
                    .getSingleResult();
        } catch (RuntimeException ex) {
            logWarn("count", ex);
            return Long.MAX_VALUE;
        }
    }

    /**
     * Retrieves all items.
     *
     * @return list of items
     */
    @Override
    public List<QuizQuestion> findAll() {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestion.FIND_ALL, QuizQuestion.class)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findAll", ex);
            return Collections.emptyList();
        }
    }

    /**
     * Retrieves an item by id.
     *
     * @param id looked id
     * @return {@code Optional} with item or empty Optional
     */
    @Override
    public Optional<QuizQuestion> findById(Long id) {
        try (Session session = openSession()) {
            logInfo("looked-up id: {}", id);
            return Optional.of(
                    session
                            .createNamedQuery(QuizQuestion.FIND_BY_ID, QuizQuestion.class)
                            .setParameter("id", id)
                            .getSingleResult());
        } catch (RuntimeException ex) {
            logWarn("findById", ex);
            return Optional.empty();
        }
    }

    /**
     * Retrieves list of items by ids.
     *
     * @param ids looked ids
     * @return list of items
     */
    @Override
    public List<QuizQuestion> findByIds(List<Long> ids) {
        try (Session session = openSession()) {
            logInfo("looked-up ids: {}", ids);
            return session
                    .createNamedQuery(QuizQuestion.FIND_BY_IDS, QuizQuestion.class)
                    .setParameter("idList", ids)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findByIds", ex);
            return Collections.emptyList();
        }
    }

    /**
     * Retrieves item by question content.
     *
     * @param content question content in form of {@code java.lang.String}
     * @return {@code Optional} with item or empty Optional
     */
    @Override
    public Optional<QuizQuestion> findByContent(String content) {
        try (Session session = openSession()) {
            logInfo("looked-up content: {}", content);
            return Optional.of(
                    session
                            .createNamedQuery(QuizQuestion.FIND_BY_CONTENT, QuizQuestion.class)
                            .setParameter("content", content)
                            .getSingleResult());
        } catch (RuntimeException ex) {
            logWarn("findByContent", ex);
            return Optional.empty();
        }
    }

    /**
     * Retrieves item by category id.
     *
     * @param id looked category id
     * @return question list
     */
    @Override
    public List<QuizQuestion> findByCategoryId(Long id) {
        try (Session session = openSession()) {
            logInfo("Looking up questions by category #{}", id);
            return session
                    .createNamedQuery(QuizQuestion.FIND_BY_CATEGORY_ID, QuizQuestion.class)
                    .setParameter("id", id)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findByCategoryId", ex);
            return Collections.emptyList();
        }
    }

    /**
     * Retrieves list of items by question category ids.
     *
     * @param ids looke category ids
     * @return list of questions
     */
    @Override
    public List<QuizQuestion> findByCategoryIds(List<Long> ids) {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestion.FIND_BY_CATEGORY_IDS, QuizQuestion.class)
                    .setParameter("categoryIdList", ids)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findByCategoryIds", ex);
            return Collections.emptyList();
        }
    }

    /**
     * Retrieves an item by Category instance.
     *
     * @param questionCategories question category instance
     * @return list
     */
    @Override
    public List<QuizQuestion> findByCategories(List<String> questionCategories) {
        try (Session session = openSession()) {
            return session
                    .createNamedQuery(QuizQuestion.FIND_BY_CATEGORY, QuizQuestion.class)
                    .setParameter("categories", questionCategories)
                    .getResultList();
        } catch (RuntimeException ex) {
            logWarn("findByCategories", ex);
            return Collections.emptyList();
        }
    }

}
